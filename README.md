# CCS / SASS Best Practices

We use [SUIT CSS](https://suitcss.github.io/) for mixins, variables, naming convention, and better readable syntax.

## Table of Contents:
- [Naming Convention](#markdown-header-naming-convention)
- [Float vs Display: inline-block](#markdown-header-float-vs-display-inline-block)
- [Box Model](#markdown-header-box-model)
- [Calc()? Math rules!](#markdown-header-calc-math-rules)
- [Sizing](#markdown-header-sizing)
- [Mixins](#markdown-header-mixins)
- [Gecko, WebKit, IE](#markdown-header-gecko-webkit-ie)
- [Check If It Will Work](#markdown-header-check-if-it-will-work)
- [CSS3 Compatibility with IE](#markdown-header-css3-compatibility-with-ie)
- [Image Responsiveness](#markdown-header-image-responsiveness)
- [Tool for Gradients](#markdown-header-tool-for-gradients)
- [Use CSS Short-hand where possible](#markdown-header-use-css-short-hand-where-possible)
- [Utilities](#markdown-header-utilities)

## Naming Convention
Refer to [SUIT CSS](https://suitcss.github.io/) for more details on the naming convention that we follow but in summary:

* ComponentName
* ComponentName-descendantName
* ComponentName--modifierName
* ComponentName.is-stateOfComponent
* u-utilityName

Also:

    // Okay, but not the best
    .ComponentName {
      width: 100%;
      ...
    }

    .ComponentName-descendantName { ... }

    .ComponentName-descendantName.is-stateOfComponent { ... }

is the same as:

    // Awesome, use this syntax!
    .ComponentName {              // .ComponentName
      width: 100%;
      ...

      &-descendantName {          // .ComponentName-descendantName

        &.is-stateOfComponent {   // .ComponentName-descendantName.is-stateOfComponent
          ...
        }
      }
    }

Both ways avoid naming collisions and the need to overwrite our CSS with specificity.
But the latter is more readable, easier to understand, and *less to type*!

**Use the second example for your CSS naming syntax!!!**

## Float vs Display: inline-block
Float - **It's a trap!**

Why is it bad? We need to use `clear:both` in order to properly position non-float elements after our floats and for the parent height to include all of its children. Why add CSS to workaround a float?

If we use a component that is not using any `float: left`, we will be spending extra time making sure it displays properly between all of the floats in our page.

Consider `display: inline-block`. No more need for `clear:both`, the parent has the correct height, most likely all other components will be positioned as expected.

Read the following articles and make your call: [CSS - inline-block](http://learnlayout.com/inline-block.html) and [float vs inline-block](http://www.ternstyle.us/blog/float-vs-inline-block)

## Box Model
Usually, there is a misunderstanding of how the box model works.

The CSS box model is:

*  width + padding + border = actual visible/rendered width of an element's box
*  height + padding + border = actual visible/rendered height of an element's box

*NOT*

* width = actual visible (including padding + border)
* height = actual visible (including padding + border)

Thus, `width: 100%; padding: 10%;` will yield an element who's *actual* width is **110%**!

What is the solution here? `box-sizing: padding-box;`

Want to make sure that `padding` **and** `border` also counts toward the width? No problem! `box-sizing: border-box;`

Stop using `width: 97%` when you use padding! Instead, use `box-sizing`. Or apply `margin`/`padding` to child elements who have `width: auto`.

Refer to these articles for more details: [Box Sizing](https://css-tricks.com/box-sizing/) and [Mozilla - Using CSS flexible boxes](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Using_CSS_flexible_boxes)

## Calc()? Math rules!
`box-sizing` doesn't help? Try: `width: calc(100% - 1em);` instead of `width: 93%` to take into account the `padding`/`border`/`margin`. It is cleaner and more accurate.

## Sizing
**Use `rem`, `em`, or `%`**. Avoid using `px` unless completely necessary.

Here is an example of an acceptable scenario for using `px`:

Read this to understand the difference between `rem` and `em`: [When to Use Em vs. Rem](http://webdesign.tutsplus.com/tutorials/comprehensive-guide-when-to-use-em-vs-rem--cms-23984)

Or if you're lazy read this summary taken from above link:

* `rem` and `em` units are computed into pixel values by the browser, based on font sizes in your design.
* `em` units are based on the font size of the element they’re used on.
* `rem` units are based on the font size of the html element.
* `em` units can be influenced by font size inheritance from any parent element
* `rem` units can be influenced by font size inheritance from browser font settings.
* use `em` units for sizing that should scale depending on the font size of an element other than the root.
* use `rem` units for sizing that doesn’t need `em` units, and that should scale depending on browser font size settings.
* use `rem` units unless you’re sure you need `em` units, including on font sizes.
* use `rem` units on media queries
* don't use `em` or `rem` in multi column layout widths - use % instead.
* don't use `em` or `rem` if scaling would unavoidably cause a layout element to break.

## Mixins
Is there CSS code that you copy and paste? **Use a mixin!**

## Gecko, WebKit, IE
Code your CSS for a Gecko browser, WebKit is most likely compatible and you will only need to worry about IE exceptions. Especially since we are already using an [autoprefixer](https://css-tricks.com/autoprefixer/).

## Double Check If It Will Work!
Not sure if the StackOverflow solution is compatible with most browsers?
**Check** to see how compatible a certain CSS or HTML property is at [Can I Use?](http://caniuse.com/) or [HTML5 please](http://html5please.com/).

## CSS3 Compatibility with IE
We are supporting IE8+

## Image Responsiveness
It is a good idea to make sure that most images are responsive/fluid. To ensure this we can use a utility.

## Tool for Gradients
Need to create a gradient or understand how gradients work? [Gradient Generator](http://www.colorzilla.com/gradient-editor/)

## Use CSS Short-hand when possible
This can make it easier to maintain our code. If we have `margin-top: 1rem;` or if we are using a variable `margin-top: $someElement-margin;`, what happens when we need to add another margin or we change a variable? For consistency it makes sense to always use `margin: 1rem 0 0 0; /* top right bottom left */`, this way no unexpected issues will arise.

## Utilities
    @include wider-than(tablet) { ... }
    @include wider-than(desktop) { ... }
    @include smaller-than(tablet) { ... }

    u-...

## ...